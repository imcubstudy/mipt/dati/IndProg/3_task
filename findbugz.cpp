/**
* @brief
*       Find errors and decrease probability of getting errors of the same kind in the future
*       This piece of code won't compile and it doesn't describe an entire algorithm: just part of some page storage
*
* @author
*       AnnaM
*/

#include <Windows.h>
#include <stdio.h>
// for memset
#include <cstring>

enum PAGE_COLOR
{
    PG_COLOR_GREEN = 1, /* page may be released without high overhead */
    PG_COLOR_YELLOW, /* nice to have */
    PG_COLOR_RED    /* page is actively used */
};


/**
 * UINT Key of a page in hash-table (prepared from color and address)
 */
union PageKey
{
    struct
    {
        CHAR    cColor: 8;
        UINT    cAddr: 24;
    };

    UINT    uKey;
};


/* Prepare from 2 chars the key of the same configuration as in PageKey */
// types mismatch
#define CALC_PAGE_KEY( Addr, Color )    (  (UINT)((Color) + ((long)Addr) << 8) )


/**
 * Descriptor of a single guest physical page
 */
struct PageDesc
{
    PageKey            uKey;

    /* list support */
    PageDesc        *next, *prev;
};

#define PAGE_INIT( Desc, Addr, Color )              \
    {   /* no candidate function to call PageKey::operator=(smth) */\
        (Desc).uKey.uKey = CALC_PAGE_KEY( Addr, Color ); \
        (Desc).next = (Desc).prev = NULL;           \
    }


/* storage for pages of all colors */
static PageDesc* PageStrg[ 3 ];

void PageStrgInit()
{
    // wrong sizeof
    memset( PageStrg, 0, sizeof(PageStrg) );
}

PageDesc* PageFind( void* ptr, char color )
{
    // empty for loop body
    for( PageDesc* Pg = PageStrg[color]; Pg; Pg = Pg->next )
        // no candidate function to call PageKey::operator=(smth)
        if( Pg->uKey.uKey == CALC_PAGE_KEY(ptr,color) )
           return Pg;
    return NULL;
}

PageDesc* PageReclaim( UINT cnt )
{
    UINT color = 0;
    // uninitialized pointer!!
    PageDesc* Pg = PageStrg[color];
    while( cnt )
    {
        Pg = Pg->next;
        PageRemove( PageStrg[ color ] );
        // not updating PageStrg array after removal
        PageStrg[color] = Pg;
        cnt--;
        if( Pg == NULL )
        {
            color++;
            Pg = PageStrg[ color ];
            // may overrun buffer if cnt > overall page amount, need extra loop exit condition
            if(color == PG_COLOR_RED) break;
        }
    }
    // control may reach end of non-void function
    return NULL;
}

PageDesc* PageInit( void* ptr, UINT color )
{
    PageDesc* pg = new PageDesc;
    if( pg )
        // PAGE_INIT expects object, not a double pointer, also, `PAGE_INIT(...);` is two expressions -> else without if
        PAGE_INIT(*pg, ptr, color)
    else
        printf("Allocation has failed\n");
    return pg;
}

/**
 * Print all mapped pages
 */
void PageDump()
{
    UINT color = 0;
    #define PG_COLOR_NAME(clr) #clr
    char* PgColorName[] =
    {
        PG_COLOR_NAME(PG_COLOR_RED),
        PG_COLOR_NAME(PG_COLOR_YELLOW),
        PG_COLOR_NAME(PG_COLOR_GREEN)
    };

    // PG_COLOR_RED is 3 (amount of colors). Here is classic buffer overrun bug
    while( color < PG_COLOR_RED )
    {
        // printf format string didn't match args order
        printf("PgStrg[(%s) %u] ********** \n", PgColorName[color], color );
        for( PageDesc* Pg = PageStrg[++color]; Pg != NULL; Pg = Pg->next )
        {
            // 'struct PageDesc' has no member named 'uAddr' and Pg is a pointer itself
            // Also using `=` for comparison (`==`)
            if( Pg == NULL )
                continue;
            // 'struct PageDesc' has no member named 'uAddr' and Pg is a pointer itself
            printf("Pg :Key = 0x%x, addr %p\n", Pg->uKey, Pg );
        }
    }
    #undef PG_COLOR_NAME
}
